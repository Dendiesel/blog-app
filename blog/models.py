from django.db import models
from django.urls import reverse


class Post(models.Model):
    title = models.CharField(max_length=50)
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE,)
    body = models.TextField()

    def __str__(self):
        return 'Автор - %s, заголовок - %s '%(self.author,self.title)

    def get_absolute_url(self):
        return reverse('post_detail', args=[str(self.id)])
        #return reverse('home')
# Create your models here.
    class Meta:
        verbose_name = 'Blog'
        verbose_name_plural = 'Blogs'
