from django.contrib import admin
from .models import Post

class AdminPost (admin.ModelAdmin):
    list_display = ['id','author', 'title' ]
    list_filter = ('author',)
    search_fields = ('author',)
    list_editable = ('author', 'title')
    def author(self,rec):
        return '%s '% (rec.author)
    author.short_description = 'Имя автора и Название'

    class Meta:
        model = Post


admin.site.register(Post, AdminPost)

# Register your models here.
